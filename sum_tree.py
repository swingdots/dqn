import numpy as np
import math

class SumTree:
    def __init__(self, memory_size):
        # tree parameters
        self.tree_level = math.ceil(math.log(memory_size+1, 2))+1
        self.tree_size = 2**self.tree_level-1
        self.tree = np.zeros(self.tree_size, dtype=np.float32)

        # first data index of tree
        self.tree_first_idx = 2**(self.tree_level-1)-1
        self.tree_last_idx = self.tree_first_idx+memory_size-1

    # convert data index to tree index
    def index_to_tree(self, data_index):
        return self.tree_first_idx + data_index
    
    # convert tree index to data index
    def index_to_data(self, tree_index):
        return tree_index - self.tree_first_idx

    # get index of left child
    def get_left_index(self, index):
        return 2*index+1

    # get index of right child
    def get_right_index(self, index):
        return 2*index+2

    # get parent index
    def get_parent_index(self, index):
        return math.ceil(index/2)-1

    # reconstruct sum tree
    def reconstruct(self, tree_index):
        left = self.tree[self.get_left_index(tree_index)]
        right = self.tree[self.get_right_index(tree_index)]
        self.tree[tree_index] = left + right

        parent_index = self.get_parent_index(tree_index)
        if parent_index >= 0:
            self.reconstruct(parent_index)
    
    # reconstruct all values of tree
    def reconstruct_all(self):
        parent_begin = self.get_parent_index(self.tree_first_idx)
        parent_end = self.tree_first_idx - 1
        for i in range(parent_begin, parent_end+1):
            self.reconstruct(i)

    # update tree value
    def update_priori(self, index, priori):
        tree_index = self.index_to_tree(index)
        self.tree[tree_index] = priori
        self.reconstruct(self.get_parent_index(tree_index))

    # find tree value
    # input value must be between 0 and 1
    def find(self, value):
        assert value >=0  and value <=1
        value *= self.tree[0]
        # find value from root
        return self._find(value, 0)

    def _find(self, value, index):
        # if node, return
        if self.tree_first_idx <= index:
            return self.index_to_data(index), self.tree[index]

        left = self.tree[self.get_left_index(index)]

        # if value of left child is greater than parent, go left
        if left >= value:
            return self._find(value, self.get_left_index(index))
        # if value of left child is lesser than parent, go right with (value-left)
        else:
            return self._find(value-left, self.get_right_index(index))

    # return max priori
    def get_max_priori(self):
        return np.max(self.tree[self.tree_first_idx:self.tree_last_idx+1])

    # return min priori
    def get_min_priori(self):
        tree_valid = self.tree[self.tree_first_idx:self.tree_last_idx+1]
        return np.min(tree_valid[np.nonzero(tree_valid)])

    def get_priori_sum(self):
        return self.tree[0]

    # print out tree data
    def print_tree(self):
        for k in range(1, self.tree_level+1):
            for j in range(2**(k-1)-1, 2**k-1):
                print(self.tree[j], end=' ')
            print()

    # save self.tree
    def save(self, path):
        path = path / 'tree.npy'
        np.save(path, self.tree)

    # load self.tree
    def load(self, path):
        path = path / 'tree.npy'
        self.tree = np.load(path)
