import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np

from atarienvironment import AtariEnvironment
from history import History

class DQN():
    def __init__(self, flags, sess, network_name):
        self.flags = flags
        self.sess = sess
        self.network_name = network_name
        self.build_model()

    def build_model(self):
        self.states =  tf.placeholder('float32',
                                      [None, self.flags.input_dim, self.flags.input_dim, self.flags.history_length],
                                      name='states')

        with tf.variable_scope(self.network_name):
            out = tf.layers.conv2d(inputs=self.states, filters=32,
                                   kernel_size=[8,8], strides=(4,4),
                                   activation=tf.nn.relu, 
                                   name='conv2d_1')
            out = tf.layers.conv2d(out, filters=64, 
                                   kernel_size=[4,4], strides=(2,2), 
                                   activation=tf.nn.relu, 
                                   name='conv2d_2')
            out = tf.layers.conv2d(out, filters=64, 
                                   kernel_size=[3,3], strides=(1, 1),
                                   activation=tf.nn.relu, 
                                   name='conv2d_3')
            out = tf.layers.flatten(out)
            
            # dueling dqn
            if self.flags.use_dueling:
                # value stream
                fc_val = tf.layers.dense(out, 512, activation=tf.nn.relu, name='fc_val')
                value = tf.layers.dense(fc_val, 1, name='output_val')

                # advantage stream
                fc_adv = tf.layers.dense(out, 512, activation=tf.nn.relu, name='fc_advantage')
                advantage = tf.layers.dense(fc_adv, self.flags.action_size, name='output_adv')

                # get q value from equation 9
                self.q = value + (advantage - tf.reduce_mean(advantage, axis=1, keep_dims=True))
            else:
                out = tf.layers.dense(out, 512, activation=tf.nn.relu, name='fc')
                self.q = tf.layers.dense(out, self.flags.action_size, name='output_layer')

        self.action_from_q = tf.argmax(self.q, axis=1)

        self.weights = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.network_name)

    def q_eval(self, state):
        return self.sess.run(self.q, feed_dict={self.states: [state]})[0]

    def action_eval(self, state):
        return self.sess.run(self.action_from_q, feed_dict={self.states: [state]})[0]

    def create_train_op(self):
        # y value
        self.y = tf.placeholder(dtype=tf.float32, shape=[None], name='y')
        # action in current state
        self.action = tf.placeholder(dtype=tf.int32, shape=[None], name='action')

        # importance-sampling weights
        self.is_weights = tf.placeholder(dtype=tf.float32, shape=[None], name='is_weight')

        # get q value of action
        action_one_hot = tf.one_hot(self.action, self.flags.action_size)
        q_acted = tf.reduce_sum(self.q * action_one_hot, reduction_indices=1)

        delta = self.y - q_acted
        # use the Huber loss for clipping delta
        clipped_delta = tf.where(tf.abs(delta) < 1.0,
                                 0.5 * tf.square(delta),
                                 tf.abs(delta) - 0.5)

        if self.flags.use_per:
            # calculate priorities
            self.priori = (tf.abs(delta)+self.flags.per_ep) ** self.flags.alpha

            # calculate loss from weighted delta
            weighted_delta = clipped_delta * self.is_weights
            self.loss = tf.reduce_mean(weighted_delta)
        else:
            self.loss = tf.reduce_mean(clipped_delta)  

        optimizer = tf.train.RMSPropOptimizer(learning_rate=self.flags.learning_rate, 
                                              momentum=self.flags.momentum, 
                                              epsilon=self.flags.epsilon)

        # gradient clipping
        if self.flags.use_clipping:
            gradients = tf.gradients(ys=self.loss, xs=self.weights)
            clipped_grads, _ = tf.clip_by_global_norm(gradients, self.flags.grad_clip)

            train_op = optimizer.apply_gradients([(grad, var) for grad, var in zip(clipped_grads, self.weights)])
        else:
            train_op = optimizer.minimize(self.loss)

        return train_op
