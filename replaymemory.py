import numpy as np
import random

class ReplayMemory:
    def __init__(self, flags):
        self.flags = flags
        self.imsize = [self.flags.input_dim, self.flags.input_dim]
        self.actions = np.empty(self.flags.memory_size, dtype=np.uint8)
        self.rewards = np.empty(self.flags.memory_size, dtype=np.float32)
        self.images = np.empty([self.flags.memory_size]+self.imsize, dtype=np.uint8)
        self.terminals = np.empty(self.flags.memory_size, dtype=np.bool)

        # size of replay memory
        self.count = 0
        # current index of buffer
        self.current = 0

        # holder for sampling of state and next state
        self.state = np.empty([self.flags.batch_size,self.flags.history_length]+self.imsize, dtype=np.float32)
        self.next_state = np.empty([self.flags.batch_size,self.flags.history_length]+self.imsize, dtype=np.float32)

    def add(self, obs, reward, terminal, action):
        self.images[self.current, ...] = obs
        self.rewards[self.current] = reward
        self.terminals[self.current] = terminal
        self.actions[self.current] = action

        # assign current count and index
        self.count = max(self.count, self.current + 1)
        self.current = (self.current + 1) % self.flags.memory_size

    def sample(self):
        indexes = []
        state_index = 0

        while True:
            index = random.randint(self.flags.history_length, self.count-1)

            # if there's true in self.terminals of cur_state index, skip index
            if self.terminals[index-self.flags.history_length:index].any():
                continue

            self.state[state_index, ...] = self.images[index-4:index, ...]
            self.next_state[state_index, ...] = self.images[index-3:index+1, ...]
            state_index += 1

            indexes.append(index)
            if len(indexes) == self.flags.batch_size:
                break
        
        reward = self.rewards[indexes]
        terminal = self.terminals[indexes]
        action = self.actions[indexes]
        
        # normalize images between 0.0 and 1.0
        self.state /= 255
        self.next_state /= 255
        # cast to float32
        state = np.transpose(self.state, [0,2,3,1]).astype(np.float32)
        next_state = np.transpose(self.next_state, [0,2,3,1]).astype(np.float32)

        return state, reward, terminal, action, next_state

    # save per_memory variables
    def save(self, path):
        save_data = {}
        save_data['actions'] = self.actions
        save_data['rewards'] = self.rewards
        save_data['terminals'] = self.terminals
        save_data['count'] = self.count
        save_data['current'] = self.current
        
        data_path = path / 'transition_data.npy'
        np.save(data_path, save_data)
        image_path = path / 'images.npy'
        np.save(image_path, self.images)

    # load per_memory variables
    def load(self, path):
        data_path = path / 'transition_data.npy'
        data = np.load(data_path)
        self.actions = data.item().get('actions')
        self.rewards = data.item().get('rewards')
        self.terminals = data.item().get('terminals')
        self.count = data.item().get('count')
        self.current = data.item().get('current')
        
        image_path = path / 'images.npy'
        self.images = np.load(image_path)        
