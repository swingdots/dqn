import random
import gym
import numpy as np

from skimage.transform import resize
from skimage.color import rgb2gray

class AtariEnvironment():
    def __init__(self, flags, is_training=True):
        self.flags = flags
        self.is_training = is_training
        self.env = gym.make(flags.env_name)
        self.action_size = self.env.action_space.n
        self.dims = (self.flags.input_dim, self.flags.input_dim)
        self.frames = []
        self.game_end = True

        self.env.reset()

    def new_game(self):
        self.frames = []

        if self.game_end:
            self.game_end = False
            self.env.reset()

        for _ in range(2):
            obs, reward, terminal, _ = self.env.step(1)
            self.frames.append(obs)
            if self.flags.is_display:
                self.env.render()

        if self.is_training:
            # random start
            num_iter = random.randrange(self.flags.no_op_max-2)
            for _ in range(num_iter):
                obs, reward, terminal, _ = self.env.step(1)
                self.frames.append(obs)
                if self.flags.is_display:
                    self.env.render()

        self.lives = self.env.unwrapped.ale.lives()
        
        return self.preprocess(obs), reward, terminal
     
    def step(self, action):
        obs, reward, terminal, _ = self.env.step(action)
        self.frames.append(obs)
        if self.flags.is_display:
            self.env.render()

        lives = self.env.unwrapped.ale.lives()
        if terminal:
            self.game_end = True
        else:
            if self.is_training and lives<self.lives:
                terminal = True    

        if not self.is_training and lives<self.lives:
            for _ in range(2):
                obs, reward, terminal, _ = self.env.step(1)
                self.frames.append(obs)
                if self.flags.is_display:
                    self.env.render()

        self.lives = lives

        return self.preprocess(obs), reward, terminal

    def preprocess(self, obs):
        # convert to gray
        gray = 0.2126*obs[:,:,0] + 0.7152*obs[:,:,1] + 0.0722*obs[:,:,2]

        # resize and casting to unsigend int
        return resize(gray, self.dims, mode='reflect').astype(np.uint8)

    def get_action_size(self):
        return self.action_size

    def get_video_frames(self):
        return np.asarray(self.frames).astype(np.uint8)
