import tensorflow as tf
import random
import numpy as np
import skvideo.io
# from skimage import io
import signal
from pathlib import Path
from tqdm import tqdm

from dqn import DQN
from history import History
from atarienvironment import AtariEnvironment
from replaymemory import ReplayMemory
from per_memory import PERMemory

random_seed = 123
tf.set_random_seed(random_seed)
random.seed(random_seed)

# # class to get kill signal
# class GracefulKiller:
#   kill_now = False
#   def __init__(self):
#     signal.signal(signal.SIGINT, self.exit_gracefully)
#     signal.signal(signal.SIGTERM, self.exit_gracefully)

#   def exit_gracefully(self,signum, frame):
#     self.kill_now = True

class Agent():
    def __init__(self, flags):
        self.flags = flags
        self.env = AtariEnvironment(self.flags)
        self.env_test = AtariEnvironment(self.flags, is_training=False)
        self.flags.action_size = self.env.get_action_size()
        self.hist = History(self.flags)
        self.sess = tf.Session()

        # initialize dqn network
        self.main_dqn = DQN(self.flags, self.sess, 'main_dqn')
        self.target_dqn = DQN(self.flags, self.sess, 'target_dqn')

        # self.killer = GracefulKiller()

    def create_copy_op(self, src_weights, target_weights, name):
        with tf.variable_scope(name):
            op_holder = []
            for src, target in zip(src_weights, target_weights):
                op_holder.append(target.assign(src.value()))
            return op_holder

    def variable_summary(self, vars):
        for var in vars:
            tf.summary.histogram('histogram_'+var.name, var)

    def train(self):
        # initialize replay memory
        if self.flags.use_per:
            memory = PERMemory(self.flags)
        else:
            memory = ReplayMemory(self.flags)

        # create copy operation
        copy_op = self.create_copy_op(self.main_dqn.weights, self.target_dqn.weights, 'copy_to_trarget_dqn')

        # create train operation
        train_op = self.main_dqn.create_train_op()

        # Set tf variable for saving step index
        tf_step = tf.Variable(0, trainable=False, dtype=tf.int32, name='step')

        # summary
        self.file_writer = tf.summary.FileWriter(self.flags.logdir, self.sess.graph)
        self.variable_summary(self.main_dqn.weights)
        merged = tf.summary.merge_all()
        
        # add saver and check whether to restore
        saver = tf.train.Saver()
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir)
        if checkpoint_path is not None:
            saver.restore(self.sess, checkpoint_path)
            start_step = self.sess.run(tf_step)
            print('restored from step: ', start_step)
            # load memory data
            print('Load memory ...')
            memory.load(Path(self.flags.memory_path))
        else:
            start_step = 0
            self.sess.run(tf.global_variables_initializer())
            # copy main_dqn to target_dqn
            self.sess.run(copy_op)

        # start a new game
        obs, _, _ = self.env.new_game()
        for _ in range(self.flags.history_length):
            self.hist.add(obs)

        reward_sum = 0
        num_episode = 0
        rewards = []
        losses = []
        q_vals = []

        do_eval = False
        do_save_memory = False
        rewards_test = []
        
        # start training
        print('Start training')
        for step in tqdm(range(start_step, self.flags.max_train_step)):
        # for step in range(start_step, self.flags.max_train_step):
            # calculate ep
            cur_ep = self.flags.ep_start - step*(self.flags.ep_start-self.flags.ep_end)/self.flags.ep_end_frame
            cur_ep = max(self.flags.ep_end, cur_ep)

            # take action from e-greedy
            cur_state = self.hist.get()
            if random.random() <= cur_ep:
                action = random.randrange(self.flags.action_size)
            else:
                action = self.main_dqn.action_eval(cur_state)

            # step action
            obs, reward, done = self.env.step(action)
            self.hist.add(obs)
            reward = max(self.flags.min_reward, min(self.flags.max_reward, reward))
            reward_sum += reward

            # store transition to replay memory
            memory.add(obs, reward, done, action)

            if done:
                # start a new game
                obs, _, _ = self.env.new_game()
                for _ in range(self.flags.history_length):
                    self.hist.add(obs)

                rewards.append(reward_sum)
                reward_sum = 0
                num_episode += 1

                if step >= self.flags.train_start_step and \
                    num_episode%self.flags.eval_every_n_episode == 0:
                    do_eval = True

            if step >= self.flags.train_start_step:
                if step%self.flags.train_freq == 0:
                    if self.flags.use_per:
                        beta = self.flags.beta + (1.0-self.flags.beta)/self.flags.max_train_step*step
                        data, indexes, weight = memory.sample(beta)
                        state, reward, terminal, action, next_state = data
                    else:
                        state, reward, terminal, action, next_state = memory.sample()
                    
                    # get q values of next state from target dqn
                    target_next_q = self.sess.run(self.target_dqn.q, feed_dict={self.target_dqn.states: next_state})

                    if self.flags.use_double:
                        # get actions from main_dqn which maximize q value of next state
                        action_from_main = self.sess.run(self.main_dqn.action_from_q, 
                                                            feed_dict={self.main_dqn.states: next_state})
                        
                        # get maximu q values from main_dqn actions
                        max_next_q = target_next_q[range(self.flags.batch_size), action_from_main]
                    else:
                        # get maximum q value
                        max_next_q = np.max(target_next_q, axis=1)

                    # convert to terminal array
                    terminal = np.array(terminal) + 0.

                    # get y value
                    y = (1-terminal)*self.flags.discount_factor*max_next_q + reward

                    if self.flags.use_per:
                        inputs = [train_op, self.main_dqn.loss, self.main_dqn.q, self.main_dqn.priori]
                        _, loss, q_val, priori = self.sess.run(inputs, feed_dict={self.main_dqn.states: state, 
                                                                                 self.main_dqn.y: y, 
                                                                                 self.main_dqn.action: action,
                                                                                 self.main_dqn.is_weights: weight})
                        
                        # update priori
                        memory.update_priori(indexes, priori)
                    else:
                        # train
                        inputs = [train_op, self.main_dqn.loss, self.main_dqn.q]
                        _, loss, q_val = self.sess.run(inputs, feed_dict={self.main_dqn.states: state, 
                                                                          self.main_dqn.y: y, 
                                                                          self.main_dqn.action: action})

                    losses.append(loss)
                    q_vals.append(q_val)

                if step%self.flags.target_update_freq == 0:
                    self.sess.run(copy_op)

            # write summary 
            if step >= self.flags.train_start_step and \
                    num_episode%self.flags.log_every_n_episode == 0:
                if num_episode==0:
                    continue

                avg_loss = np.mean(losses)
                avg_q_val = np.mean(q_vals)
                avg_reward = np.mean(rewards)
                max_reward = np.max(rewards)
                min_reward = np.min(rewards)

                # Save summaries
                summary = tf.Summary(value=[
                    tf.Summary.Value(
                        tag='train/loss', simple_value=float(avg_loss)), 
                    tf.Summary.Value(
                        tag='train/q_val', simple_value=float(avg_q_val)),
                    tf.Summary.Value(
                        tag='train/avg_reward', simple_value=float(avg_reward)),
                    tf.Summary.Value(
                        tag='train/max_reward', simple_value=float(max_reward)),
                    tf.Summary.Value(
                        tag='train/min_reward', simple_value=float(min_reward)),
                    tf.Summary.Value(
                        tag='train/epsilon', simple_value=float(cur_ep)),
                ])
                self.file_writer.add_summary(summary, step)

                summary = self.sess.run(merged)
                self.file_writer.add_summary(summary, step)

                # add beta to summary
                if self.flags.use_per:
                    summary = tf.Summary(value=[
                        tf.Summary.Value(
                            tag='train/per_beta', simple_value=float(beta)), 
                    ])
                    self.file_writer.add_summary(summary, step)

                print('\n=> Training Result')
                print('step: {}, loss: {:.6f}, q_val: {:.6f}'.format(step, avg_loss, avg_q_val))
                print('avg reward: {:.6f}, min reward: {}, max reward: {}'.format(avg_reward, min_reward, max_reward))
                if self.flags.use_per:
                    print('beta: {:.6f}, max_priori: {:.6f}, min_priori: {:.6f}'.format(
                                beta, memory.max_priori, memory.min_priori))

                losses = []
                q_vals = []
                rewards = []
                num_episode = 0

            # do evaluation
            if do_eval:
                do_eval = False
                rewards_test.append(self.eval())

                if len(rewards_test)==self.flags.log_every_n_eval:
                    avg_reward = np.mean(rewards_test)
                    max_reward = np.max(rewards_test)
                    min_reward = np.min(rewards_test)

                    # Save evaluation summaries
                    summary = tf.Summary(value=[
                        tf.Summary.Value(
                            tag='eval/avg_reward', simple_value=float(avg_reward)),
                        tf.Summary.Value(
                            tag='eval/max_reward', simple_value=float(max_reward)),
                        tf.Summary.Value(
                            tag='eval/min_reward', simple_value=float(min_reward)),
                    ])
                    self.file_writer.add_summary(summary, step)

                    print('\n=> Evaluation result at step: ', step)
                    print('avg reward: {:.6f}, min reward: {}, max reward: {}'.format(avg_reward, min_reward, max_reward))

                    rewards_test = []

            # save model and memory every self.flags.save_every_n_step steps
            if step>=self.flags.train_start_step and step%self.flags.save_every_n_step==0:
                do_save_memory = True

            # when application gets kill signal and terminal state or every at 1,000,000 steps
            # if done and (self.killer.kill_now or do_save_memory):
            if done and do_save_memory:
                do_save_memory = False

                # Save model
                self.sess.run(tf.assign(tf_step, step))
                saver.save(self.sess, self.flags.check_point_file)

                # # save per_memory and sum_tree
                # print('\nSaving memory data ...')
                # path = Path(self.flags.memory_path)
                # if not path.exists():
                #     path.mkdir(parents=True)
                # memory.save(path)

                # if self.killer.kill_now:
                #     break

    # evaluation atari game with is_training=False option
    def eval(self):
        hist = History(self.flags)
        reward_sum = 0

        # start a new game
        obs, _, _ = self.env_test.new_game()
        for _ in range(self.flags.history_length):
            hist.add(obs)

        while True:
            # take action from e-greedy
            if random.random() <= self.flags.ep_test:
                action = random.randrange(self.flags.action_size)
            else:
                action = self.main_dqn.action_eval(hist.get())

            # step action
            obs, reward, terminal = self.env_test.step(action)
            hist.add(obs)
            reward_sum += reward

            if terminal:
                break

        return reward_sum

    def test(self):
        tf_step = tf.Variable(0, trainable=False, dtype=tf.int32, name='step')
        
        # add saver and check whether to restore
        saver = tf.train.Saver()
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir)
        if checkpoint_path is not None:
            saver.restore(self.sess, checkpoint_path)
            step = self.sess.run(tf_step)
            print('restored from step: ', step)
        else:
            print('None path')
            return

        num_game = 50
        best_reward = 0
        best_frames = []

        env = AtariEnvironment(self.flags, is_training=False)

        for i in range(num_game):
            # start a new game
            obs, _, _ = env.new_game()
            for _ in range(self.flags.history_length):
                self.hist.add(obs)

            reward_sum = 0

            while True:
                # take action from e-greedy
                if random.random() <= self.flags.ep_test:
                    action = random.randrange(self.flags.action_size)
                else:
                    action = self.main_dqn.action_eval(self.hist.get())

                # step action
                obs, reward, terminal = env.step(action)
                self.hist.add(obs)
                reward_sum += reward

                if terminal:
                    break

            if reward_sum > best_reward:
                best_reward = reward_sum
                best_frames = env.get_video_frames()

            print('episode: {}, reward: {}, best reward: {}'.format(i, int(reward_sum), int(best_reward)))

        if self.flags.save_video:
            skvideo.io.vwrite('result_'+str(int(best_reward))+'.mp4', best_frames)
            # skvideo.io.vwrite('result_step'+str(int(max_step))+'.mp4', max_step_frames)
            # skvideo.io.vwrite('result_min_'+str(int(min_reward))+'.mp4', min_frames)

    # save test results for DQfD
    def save_demonstration(self):
        tf_step = tf.Variable(0, trainable=False, dtype=tf.int32, name='step')
        
        # add saver and check whether to restore
        saver = tf.train.Saver()
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir)
        if checkpoint_path is not None:
            saver.restore(self.sess, checkpoint_path)
            step = self.sess.run(tf_step)
            print('restored from step: ', step)
        else:
            print('None path')
            return

        images = []
        rewards = []
        actions = []
        terminals = []
        frames = []
        reward_sum = 0

        env = AtariEnvironment(self.flags)
        
        for i in range(10):
            # start a new game
            obs, _, _ = env.new_game()
            for _ in range(self.flags.history_length):
                self.hist.add(obs)

            while True:
                # take action from e-greedy
                if random.random() <= self.flags.ep_test:
                    action = random.randrange(self.flags.action_size)
                else:
                    action = self.main_dqn.action_eval(self.hist.get())

                # step action
                obs, reward, terminal = env.step(action)
                self.hist.add(obs)
                reward_sum += reward

                images.append(obs)
                rewards.append(reward)
                actions.append(action)
                terminals.append(terminal)

                if terminal:
                    break

            print('step: {}, reward: {}'.format(i, int(reward_sum)))

            # get video data
            if i%5==0:
                frames = env.get_video_frames()
            else:
                frames = np.concatenate((frames,env.get_video_frames()), axis=0)

            if (i+1)%5==0:
                print('Saving data ...')

                # save data
                filename = 'transition_data_' + str(int(reward_sum)) + '_' + str(len(images))
                data = {}
                data['images'] = images
                data['rewards'] = rewards
                data['actions'] = actions
                data['terminals'] = terminals
                np.save(filename, data)

                # save video
                skvideo.io.vwrite(filename + '.mp4', frames)

                images = []
                rewards = []
                actions = []
                terminals = []
                frames = []
                reward_sum = 0

                print('Restart game')
